create table if not exists professor_disciplina(id int not null auto_increment primary key, disciplina varchar(200), numero_uc int, professor varchar(200));

insert into professor_disciplina(disciplina, numero_uc, professor) values ('montagem_manutencao', 1, 'alberçanto');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('formatacao', 2, 'alberçanto');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('manutencao', 3, 'alberçanto');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('projeto_integrador', 4, 'alberçanto');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('redes_locais', 5, 'alberçanto');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('manutencao_redes_locais', 6, 'alberçanto');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('monitoramento_redes_locais', 7, 'alberçanto');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('projeto_integrador', 8, 'alberçanto');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('logica_programacao', 9, 'cleber');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('codificacao_desktop', 10, 'cleber');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('codificacao_dispositivos_moveis', 11, 'cleber');
insert into professor_disciplina(disciplina, numero_uc, professor) values ('java_web', 12, 'daniel');

drop table professor_disciplina;



