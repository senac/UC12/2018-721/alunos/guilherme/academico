use banco1;

create table if not exists boletim(id int not null auto_increment primary key, id_aluno int, notas int);

drop table boletim;

create table boletim(id int not null auto_increment primary key, id_aluno int, notas int);

select * from boletim;

-- inserindo nota do primeiro aluno

insert into boletim(id_aluno, notas) values(1, 10);
insert into boletim(id_aluno, notas) values(1, 9.5);
insert into boletim(id_aluno, notas) values(1, 10);

-- inserindo nota do segundo aluno

insert into boletim(id_aluno, notas) values(2, 7);
insert into boletim(id_aluno, notas) values(2, 5);
insert into boletim(id_aluno, notas) values(2,10);

-- inserindo nota do terceiro aluno

insert into boletim(id_aluno, notas) values(3, 8);
insert into boletim(id_aluno, notas) values(3, 9);


