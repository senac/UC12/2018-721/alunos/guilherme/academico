create table if not exists disciplinas(id int not null auto_increment primary key, disciplina varchar(200), numero_uc int);



insert into disciplinas (disciplina, numero_uc) values('montagem_manutencao', 1);
insert into disciplinas (disciplina, numero_uc) values('formatacao', 2);
insert into disciplinas(disciplina, numero_uc) values('manutencao', 3);
insert into disciplinas(disciplina, numero_uc) values('projeto_integrador', 4);
insert into disciplinas(disciplina, numero_uc) values('redes_locais', 5);

insert into disciplinas(disciplina, numero_uc) values('manutencao_redes_locais', 6);
insert into disciplinas(disciplina, numero_uc) values('monitoramento_redes_locais', 7);
insert into disciplinas(disciplina, numero_uc) values('projeto_integrador', 8);
insert into disciplinas(disciplina, numero_uc)values('logica_programacao', 9);
insert into disciplinas(disciplina, numero_uc) values('codificacao_desktop', 10);
insert into disciplinas(disciplina, numero_uc) values('codificacao_dispositivos_moveis', 11);
insert into disciplinas(disciplina, numero_uc) values('java_web', 12);


drop table disciplinas;
