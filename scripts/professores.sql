create table if not exists professor(id int not null auto_increment primary key, professor varchar(200), numero_uc int);

insert into professor(professor, numero_uc) values('alberçanto', 1);
insert into professor(professor, numero_uc) values('alberçanto', 2);
insert into professor(professor, numero_uc) values('alberçanto', 3);
insert into professor(professor, numero_uc) values('alberçanto', 4);
insert into professor(professor, numero_uc) values('alberçanto', 5);
insert into professor(professor, numero_uc) values('alberçanto', 6);
insert into professor(professor, numero_uc) values('alberçanto', 7);
insert into professor(professor, numero_uc) values('alberçanto', 8);
insert into professor(professor, numero_uc) values('cleber', 9);
insert into professor(professor, numero_uc) values('cleber', 10);
insert into professor(professor, numero_uc) values('cleber', 11);
insert into professor(professor, numero_uc) values('daniel', 12);

drop table professor;